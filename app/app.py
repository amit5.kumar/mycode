from flask import Flask,request
import os
import math
app = Flask(__name__)

@app.route('/')
def hello_name():
   mynum = int(request.args.get("num"))
   return f"<h2>{math.sqrt(mynum)}</h2>"

if __name__ == '__main__':
   app.run(host='0.0.0.0',port=5000)